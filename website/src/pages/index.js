/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from "react";
import classnames from "classnames";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

const features = [
  {
    title: <>Fast</>,
    imageUrl: "img/speed.png",
    description: (
      <>
        Caravaggio aims to be as fast as possible. An internal cache system and
        the ability to pair with CDNs make it blazing fast!
      </>
    )
  },
  {
    title: <>Easy to use, highly configurable</>,
    imageUrl: "img/easy.png",
    description: (
      <>
        Start caravaggio, it simply works. The basic configuration is good for
        most use cases but you can tweak it for your needs.
      </>
    )
  },
  {
    title: <>Seamless integration</>,
    imageUrl: "img/integration.png",
    description: (
      <>
        It has been designed, to be easily integrated with any other brick of
        your system: caches, CDNs, web-proxies, orchestrators...
      </>
    )
  },
  {
    title: <>Reliable</>,
    imageUrl: "img/reliable.png",
    description: <>Clear error messages, robust and easily maintanable code.</>
  }
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames("col col--6", styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h2>{title}</h2>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Caravaggio - image manipulation service"
    >
      <header className={classnames("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <img src="img/caravaggio-logo.png" alt="Caravaggio logo" width="180"/>
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttonsContainer}>
            <div className={styles.buttons}>
              <Link
                className={classnames(
                  "button button--outline button--secondary button--lg",
                  styles.getStarted
                )}
                to={useBaseUrl("docs/whatis")}
              >
                About
              </Link>
            </div>
            <div className={styles.buttons}>
              <Link
                className={classnames(
                  "button button--outline button--secondary button--lg",
                  styles.getStarted
                )}
                to={useBaseUrl("docs/install")}
              >
                Install
              </Link>
            </div>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
