---
id: help
title: Getting help
sidebar_label: Getting help
---

If you have any problem using Caravaggio or if you find a bug, please report an <a href="https://gitlab.com/ramiel/caravaggio/issues" target="_blank">issue on gitlab</a> or <a href="mailto:incoming+ramiel-caravaggio-5291351-issue-@incoming.gitlab.com" target="_blank">send an email</a>.

You can also talk directly with us using the chat icon in the bottom right corner of this website, when available.

We'll be happy to help you.
