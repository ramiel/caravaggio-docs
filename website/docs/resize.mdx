---
id: resize
title: Resize
sidebar_label: Resize
---
import ExampleBlock from './components/ExampleBlock';

Resize can be a very complex task in Caravaggio. There are plenty of ways of resizing, cropping and extract portion of your images.    
In general a resize command take this shape

**`rs,s:<size>,m:<mode>,iar:<true|false>,b:<background color>,g:<gravity>`**

Where:     
__rs__ means you want change the final dimension of your image.    
__s__ is the new size (it can be expressed in several ways as explaned [here](#sizes)).    
__m__ define how to resize. Maybe you want to stretch the image, or take just a portion. See below;    
Some parameters apply to some modes only    
__b__ background color, in case the new image won't fit the entire viewport
__g__ is the gravity to decide the new position
__iar__ mean Ignore Aspect Ratio. Some transformations let you stretch the image without keeping the original proportions

## Resize methods

### Scale

In scale mode the image will have the dimension of the size you specified. The aspect ratio is kept unless you pass __iar__ (ignore aspect ratio) as additional parameter.    
Since `scale` is the default resize mode, you can avoid specifiying it.

Scale the image to 200x300 px keeping the aspect ratio

<ExampleBlock operation="rs,s:200x300" />  

Scale the image to 200 px width keeping the aspect ratio. The height is calculated automatically    

<ExampleBlock operation="rs,s:200" />  


Scale the image to 200x300 px width ignoring the aspect ratio.

<ExampleBlock operation="rs,s:200x300,m:scale,iar" />  

**Original**     
![Two girls](/assets/example/girls.jpeg)

**Resized**   
![Smaller image of two girls](/assets/example/girls_small.jpeg)

### Fit

The image will take the maximum available space up to the specified size, keeping the aspect ratio.    
For example this will resize the image up to 300x300 pixels and all the image will be visible.

<ExampleBlock operation="rs,s:300x300,m:fit" />

![Two girls, the image is fitted](/assets/example/girls_300x300_fit.jpeg)


**NOTE**: both width and height must be passed. Nonetheless they can be expressed in percentage

### Down fit

Like fit but only if the image is **larger** than the desired size (width _or_ height), otherwise it will be left untouched.

<ExampleBlock operation="rs,s:300x300,m:downfit" />

**NOTE**: both width and height must be passed. Nonetheless they can be expressed in percentage

### Up fit

Like fit but only if the image is **smaller** than the desired size (__both__ the width and height must be smaller), otherwise it will be left untouched.

<ExampleBlock operation="rs,s:300x300,m:upfit" />

**NOTE**: both width and height must be passed. Nonetheless they can be expressed in percentage

### Fill

Produce a new image of specified width and height. The original image is then filled inside those dimensions. The excess part will be excluded.    
You can specify a [gravity](#gravity) to decide which part of the original image should be excluded. If nothing is specified the default gravity is `center`.

In this example we fill the image in a 300x300 px space, with gravity on the center

<ExampleBlock operation="rs,s:300x300,m:fill" />

![Two girls, image is filled. Gravity is on the center](/assets/example/girls_300x300_fill_center.jpeg)

Here instead we change the gravity to north-east

<ExampleBlock operation="rs,s:300x300,m:fill,g:ne" />

![Two girls, image is filled with north east gravity](/assets/example/girls_300x300_fill_ne.jpeg)

`Fill` also accept a special gravity parameter: <span id="auto">**`auto`**</span>. This let you automatically select a meaningfull region of the image.    
Here an example

Original    
<a href="https://cvg-res.now.sh/fill_original.png" target="_blank" alt="A girl in the wind">

![One girl. She is on the left side of the picture. The rest of the image has no particulars](/assets/example/fill_original.png)

</a>

Gravity on center (**rs_200x300_fill_center**)  
<a href="https://caravaggio.now.sh/rs,s:200x300,m:fill,g:center/https://cvg-res.now.sh/fill_original.png" target="_blank" alt="A girl in the wind, cropped">

![One girl, filled with gravity on the center](/assets/example/fill_center.png)

</a>

Gravity auto (**rs_200x300_fill_auto**)  
<a 
  href="https://caravaggio.now.sh/rs,s:200x300,m:fill,g:auto?image=https://cvg-res.now.sh/fill_original.png" 
  target="_blank" 
  alt="A girl in the wind. The image is centered on the face"
>
  <img alt="One girl, filled auto, the girl is now centered in the image" src="/assets/example/fill_auto.png" />
</a>

<ExampleBlock operation="rs,s:200x300,m:fill,g:auto" targetImg="https://cvg-res.now.sh/fill_original.png" />


### Down fill

Same as fill but only if the image is larger than the target size (width _and_ height).

<ExampleBlock operation="rs,s:300x300,m:downfill" />

<ExampleBlock operation="rs,s:300x300,m:downfill,g:west" />

### Embed

This embeds the image in the specified dimensions. The entire image is taken and aspect ratio is kept. If the image doesn't
fit well in the dimension the output will be padded with a color of your choice (or black by default).
A [gravity](#gravity) can be specified to decide how to embed the image. Both `gravity` and `background color` are optional

<ExampleBlock operation="rs,s:320x240,m:embed" />

![Two girls. The picture is embedded and the remaining space is black](/assets/example/girls_embed.jpeg)

You can specify the color in RGB (red: 105, green: 72, blue: 200). Each number must have 3 digits, you have to pad them (so 72 must be written as 072).

<ExampleBlock operation="rs,s:320x240,m:embed,b:105072200" />

![Two girls. The picture is embedded and the remaining space is purple](/assets/example/girls_embed_color.jpeg)

The color can be specified in RGBA. To add the alpha channel value append `.n` at the end. To have full transparency put `.0`, to have full opacity put nothing, just the color.    

<ExampleBlock operation="rs,s:320x240,m:embed,b:105072200.5" />

The color can be specified in hexadecimal code (6 digit). Alpha can be specified as well

<ExampleBlock operation="rs,s:320x240,m:embed,b:FF5400.5" />

You can spcify the gravity     

<ExampleBlock operation="rs,s:320x240,m:embed,b:FF5400,g:north" />

![Two girls. The picture is embedded with gravity on north](/assets/example/girls_embed_gravity.jpeg)

## Sizes

The size parameter can be specified in several format in each resize mode. 

### Pixel

`rs,s:200x300` is 200 x 300 px    
`rs,s:200` is 200 px width, height auto    
`rs,s:x300` is 200 px height, width auto    

### Percentage

You can specify the width and height as percentage value of the input image

`rs,s:0.2x0.3` is 20% of the width and 30% of the height    
`rs,s:0.2` is 20% of the width    
`rs,s:x0.3` is 30% of the height  

### Mixed

Pixel and percentage can be mixed.

`rs,s:200x0.3` is 200px of width and 30% of height

## Gravity

For those operations which support gravity, those value can be specified

`center`, `north`, `northeast`, `northwest`, `east`, `west`, `south`, `southeast`, `southwest`

and can be abbreviated in 

`c`, `n`, `ne`, `nw`, `e`, `w`, `s`, `se`, `sw`.

Some operations, such as `fill` and `downfill`, accept the `auto` gravity. This automatically focus on the most important region of the image. 
Look [here](#auto) for an example.

## Colors

The colors accepetd by some operations can be expressed as:

RGB `120230007` in this case the rgb(120, 230, 7)

The three values of red, green and blue must be put together and each must be of three digits, so 7 becomes 007 and 45 becomes 045.

Hexadecimal `FF00AB` is in the hex format. The short notation (3 digits only) is not accepted yet and `#` at the beginning must be removed.

To specify the opacity you can add `.N` at the end. So to set an opacity of 0.4 the previouses become `120230007.4` and `FF00AB.4`    
Omitting the opacity means it is 1, full opaque, while `.0` means full transparent.
