---
id: programmatic
title: Programmatic use
sidebar_label: Programmatic use
---

Caravaggio can be used programmatically as an node library.

In your project

```
npm i caravaggio
```

Caravaggio exposes a function that accepts a `ServerRequest` and a `ServerResponse`. You can use any webserver you like, or none. Let's see an example written in typescript.


```ts
import caravaggio from 'caravaggio';


```
