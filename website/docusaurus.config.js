/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
const path = require('path');
const BASE_URL = '/';
const versions = require('./versions.json');

module.exports = {
  title: 'Caravaggio',
  tagline: 'A blazing fast image manipulation service',
  url: 'https://caravaggio.ramielcreations.com',
  baseUrl: BASE_URL,
  favicon: 'img/favicon.ico',
  organizationName: 'ramielcreations', // Usually your GitHub org/user name.
  projectName: 'caravaggio', // Usually your repo name.
  onBrokenLinks: 'warn',
  themeConfig: {
    algolia: {
      apiKey: "f9d826a898ef4151cf158bddb87a3de8",
      indexName: "ramielcreations_caravaggio",
      algoliaOptions: {}, // Optional, if provided by Algolia
    },
    gtagmanager: {
      trackingID: 'GTM-5DP3BRH',
    },
    image: 'img/caravaggio-logo.png',
    navbar: {
      title: 'Caravaggio',
      logo: {
        alt: 'Caravaggio logo',
        src: 'img/caravaggio-logo.png',
      },
      items: [
        {
          label: 'Docs',
          to: 'docs', // "fake" link
          position: 'left',
          activeBaseRegex: `docs/(?!next/(support|team|resources))`,
          items: [
            {
              label: `${versions[0].replace('.0.0','.x')} (latest)`,
              to: 'docs',
              activeBaseRegex: `docs/(?!${versions.join('|')}|next)`,
            },
            ...versions.slice(1).map((version) => ({
              label: version,
              to: `docs/${version}`,
            })),
            {
              label: 'Next (not yet released)',
              to: 'docs/next',
              activeBaseRegex: `docs/next/(?!support|team|resources)`,
            },
          ],
        },
        // {to: 'blog', label: 'Blog', position: 'left'},
        { 
          to: "https://gitlab.com/ramiel/caravaggio", 
          className: 'header-github-link',
          position: 'right',
          'aria-label': 'GitLab repository',
        },
        { to: 'docs/help', label: "Help" }
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs & Code',
          items: [
            {
              label: 'Docs',
              to: 'docs/whatis',
            },
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/ramiel/caravaggio',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'RamielCreations',
              href: 'https://www.ramielcreations.com',
            },
            {
              label: 'License',
              to: 'docs/license',
            },
          ],
        },
        // {
        //   title: 'Social',
        //   items: [
        //     {
        //       label: 'Blog',
        //       to: 'blog',
        //     },
        //   ],
        // },
      ],
      logo: {
        alt: 'Caravaggio @ Ramielcreations',
        src: 'img/gomitolo.png',
      },
      copyright: `Made with 💚 by Fabrizio Ruggeri @ ramielcreations`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // homePageId: 'whatis',
          // path: 'docs',
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        sitemap: {
          cacheTime: 600 * 1000, // 600 sec - cache purge period
          changefreq: 'weekly',
          priority: 0.5,
        }
      },
    ],
  ],
  plugins: [path.resolve(__dirname, 'src/plugin-google-gtagmanager')],
};
