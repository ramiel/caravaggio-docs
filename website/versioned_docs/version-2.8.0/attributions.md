---
id: attributions
title: Attributions
sidebar_label: Attributions
---

The images used in this documentation are taken on [Pexels](https://www.pexels.com), by its 
amazing photographers!

Here all the links to the original photos:
- [Photo by Matheus Bertelli from Pexels](https://www.pexels.com/photo/blur-casual-christmas-lights-close-up-573306/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)
- [Photo by Simon Migaj from Pexels](https://www.pexels.com/photo/person-on-a-bridge-near-a-lake-747964/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)
- [Photo by Klayfe Rohden from Pexels](https://www.pexels.com/photo/baked-pies-and-breads-873653/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)
- [Photo by Emma Bauso from Pexels](https://www.pexels.com/photo/family-of-four-walking-at-the-street-2253879/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)
