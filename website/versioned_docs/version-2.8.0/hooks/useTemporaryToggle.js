import { useState } from 'react';

/**
 * Set a value to true and automatically set it back to false after `time` elapsed
 * @param time The time to wait before resetting
 * @returns [isOn, setOn] where `isOn` is the current toggle value and `setOn` set the toggle to true
 */
const useTemporaryToggle = (time = 500) => {
  const [isOn, setIsOn] = useState(false);
  let timer;

  return [
    isOn,
    () => {
      if (timer) {
        clearTimeout(timer);
      }
      setIsOn(true);
      timer = setTimeout(() => {
        setIsOn(false);
      }, time);
    },
  ];
};

export default useTemporaryToggle;
