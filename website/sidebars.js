/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  'docs': {
    'Getting started': [
      'whatis',
      'install',
      'configuration',
      'help'
    ],
    'Usage': [
      'general-usage',
      'output',
      'progressive',
      'resize',
      'extract',
      'overlay',
      'quality',
      'rotate',
      'flip',
      'blur',
      'duotone'
    ],
    'Guides': [
      'deployments',
      'programmatic',
      'nextjs'
    ],
    'More': [
      'attributions',
      'license'
    ]
  }
}
